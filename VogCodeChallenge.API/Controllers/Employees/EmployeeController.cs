﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VogCodeChallenge.API.Interfaces;

namespace VogCodeChallenge.API.Controllers.Employees
{
    [ApiController]
    [Route("api/employees")]
    public class EmployeeController:Controller
    {
        private readonly IEmployeeService _employeeService;

        
        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetList()
        {
            var result = await _employeeService.ListAll();
            return !result.Errors.Any() ? Ok(result.Data) : BadRequest(result.Errors);
        }

        [HttpGet]
        [Route("department/{departmentId}")]
        public async Task<IActionResult> GetEmployeesByDepartmentId(int departmentId)
        {
            var result = await _employeeService.GetEmployeesByDepartmentId(departmentId);
            return !result.Errors.Any() ? Ok(result.Data) : BadRequest(result.Errors);
        }
    }
}
