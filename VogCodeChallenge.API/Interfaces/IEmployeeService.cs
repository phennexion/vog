﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VogCodeChallenge.API.Models;

namespace VogCodeChallenge.API.Interfaces
{
    public interface IEmployeeService
    {
        Task<ResultModel> GetAll();
        Task<ResultModel> ListAll();
        Task<ResultModel> GetEmployeesByDepartmentId(int id);
    }
}
