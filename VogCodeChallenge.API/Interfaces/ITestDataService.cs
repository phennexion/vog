﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VogCodeChallenge.API.Models;

namespace VogCodeChallenge.API.Interfaces
{
    public interface ITestDataService
    {
        Task<List<Department>> Departments();
        Task<List<Employee>> EmployeeList();
        Task<List<Employee>> Employees();
    }
}
