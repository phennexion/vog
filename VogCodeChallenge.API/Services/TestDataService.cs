﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VogCodeChallenge.API.Interfaces;
using VogCodeChallenge.API.Models;

namespace VogCodeChallenge.API.Services
{
    public class TestDataService: ITestDataService
    {
        public async Task<List<Department>> Departments()
        {
            var departments = new List<Department>();
            var department1 = new Department();
            var employeeList = await Employees();
            department1.Id = 1;
            department1.Name = "Engineering";
            department1.Address = "101 6 Avenue SW, Calgary, AB, Canada T2C 5N4";
            department1.Employees = employeeList;
            departments.Add(department1);
            
            return departments;
        }

        public async Task<List<Employee>> Employees()
        {
            var employees = new List<Employee>();

            var employee1 = new Employee();
            employee1.Id = 1;
            employee1.FirstName = "Mitch";
            employee1.LastName = "Matula";
            employee1.Title = "Developer";
            employee1.Address = "123 Fairview Street NW, Calgary, AB, Canada, T2C 5N4";
            var employee2 = new Employee();
            employee2.Id = 2;
            employee2.FirstName = "Kiomi";
            employee2.LastName = "Sakata";
            employee2.Title = "Head Chef";
            employee2.Address = "800 Mountain View Crescent SW, Calgary, AB, Canada, T1A 3E3";
            var employee3 = new Employee();
            employee3.Id = 3;
            employee3.FirstName = "Matt";
            employee3.LastName = "Corbet";
            employee3.Title = "Helpdesk Analyst";
            employee3.Address = "#120 34 Avenue SE, Calgary, AB, Canada, T2E 2T2";
            employees.Add(employee1);
            employees.Add(employee2);
            employees.Add(employee3);

            return employees;
        }

        public async Task<List<Employee>> EmployeeList()
        {
            var employeeList = new List<Employee>();

            var employee1 = new Employee();
            employee1.FirstName = "Mitch";
            employee1.LastName = "Matula";
            var employee2 = new Employee();
            employee2.FirstName = "Kiomi";
            employee2.LastName = "Sakata";
            var employee3 = new Employee();
            employee3.FirstName = "Matt";
            employee3.LastName = "Corbet";
            employeeList.Add(employee1);
            employeeList.Add(employee2);
            employeeList.Add(employee3);

            return employeeList;
        }

        

    }
}
