﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VogCodeChallenge.API.Interfaces;
using VogCodeChallenge.API.Models;

namespace VogCodeChallenge.API.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly ITestDataService _testDataService;

        public EmployeeService(ITestDataService testDataService)
        {
            _testDataService = testDataService;
        }

        public async Task<ResultModel> ListAll()
        {
            var employeeList = await _testDataService.EmployeeList();

            if (employeeList == null)
            {
                return new ResultModel
                {
                    Errors = new List<string> {"Did not find employee list."}
                };
            }
            return new ResultModel
            {
                Data = employeeList
            };
        }

        public async Task<ResultModel> GetAll()
        {
            var employees = await _testDataService.Employees();

            if (employees == null)
            {
                return new ResultModel
                {
                    Errors = new List<string> { "Did not find employees." }
                };
            }
            return new ResultModel
            {
                Data = employees
            };
        }

        public async Task<ResultModel> GetEmployeesByDepartmentId(int id)
        {
            var departments = await _testDataService.Departments();
            var department = departments.FirstOrDefault(x => x.Id == id);
            if (department == null)
            {
                return new ResultModel
                {
                    Errors = new List<string> { "Did not find a department with that Id." }
                };
            }
            return new ResultModel
            {
                Data = department
            };
        }

    }
}
