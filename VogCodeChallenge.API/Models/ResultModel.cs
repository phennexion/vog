﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VogCodeChallenge.API.Models
{
    public class ResultModel<T>
    {
        public T Data { get; set; }
        public int Total { get; set; }
        public IList<string> Errors { get; set; }
        public string Message { get; set; }
        public bool IsSuccess => !Errors.Any() && Total > -1;

        public ResultModel()
        {
            Errors = new List<string>();
            Message = "";
        }
    }

    public class ResultModel : ResultModel<dynamic> { }
}
